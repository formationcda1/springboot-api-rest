package com.simplon.apirest.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.simplon.apirest.entity.Book;
import com.simplon.apirest.repository.BookRepository;

import jakarta.persistence.EntityNotFoundException;

@Service
public class BookService {
    private BookRepository bookRepository;

    public BookService(BookRepository bookRepositoryInjected) {
        this.bookRepository = bookRepositoryInjected;
    }

    public List<Book> getAllBooks() {
        return this.bookRepository.findAll();
    }

    
    public Book getBookById(Long bookId) {
        Optional<Book> optionalBook = this.bookRepository.findById(bookId);
        if (optionalBook.isPresent()) {
            return optionalBook.get();
        } else {
            // possibilité orElseThrow() qui  est une méthode de la classe Optional qui permet de lever une exception si l'Optional ne contient pas de valeur
            throw new EntityNotFoundException("Aucun livre détenant l'id : " + bookId);
        }
    }

    public Book createBook(Book book) {
        return this.bookRepository.save(book);
    }

    public Book updateBook(Long id, Book bookDetails) {
        Book book = getBookById(id);

        if (bookDetails.getTitle() != null) {
            book.setTitle(bookDetails.getTitle());
        }

        if (bookDetails.getDescription() != null) {
            book.setDescription(bookDetails.getDescription());
        }

        book.setAvailable(bookDetails.isAvailable());

        return bookRepository.save(book);
    }

    public void deleteBook(Long id) {
        Book book = getBookById(id);
        bookRepository.delete(book);
    }

    
    public Optional<Book> findByTitle(String title) {
        return bookRepository.findByTitle(title);
    }

    
}
