package com.simplon.apirest.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simplon.apirest.entity.Category;
import com.simplon.apirest.repository.CategoryRepository;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }
    // Récupère toutes les catégories depuis la base de données
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }
    // Récupère une catégorie par son ID
    public Category getCategoryById(Long id) {
        // Utilise Optional pour éviter les NullPointerExceptions
        Optional<Category> category = categoryRepository.findById(id);
        return category.orElse(null);// Retourne la catégorie si elle existe, sinon null
    }

    // Enregistre une nouvelle catégorie dans la base de données
    public Category createCategory(Category category) {
        return categoryRepository.save(category);
    }

     // Supprime une catégorie par son ID
    public void deleteCategory(Long id) {
        categoryRepository.deleteById(id);
    }
}
