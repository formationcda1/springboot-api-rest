package com.simplon.apirest.service;

import java.util.List;


import org.springframework.stereotype.Service;

import com.simplon.apirest.entity.Author;
import com.simplon.apirest.repository.AuthorRepository;

@Service
public class AuthorService {
    private final AuthorRepository authorRepository;

    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }
    public List<Author> getAllAuthors() {
        return this.authorRepository.findAll();
    }
}
