package com.simplon.apirest.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.simplon.apirest.entity.Book;
import com.simplon.apirest.entity.Category;
import com.simplon.apirest.repository.BookRepository;
import com.simplon.apirest.repository.CategoryRepository;
import com.simplon.apirest.service.BookService;

import jakarta.persistence.EntityNotFoundException;

@RestController
public class BookController {

    private BookService bookService;
    private BookRepository bookRepository;
    private CategoryRepository categoryRepository;

    public BookController(BookService bookServiceInjected) {
        this.bookService = bookServiceInjected;
    }

    @GetMapping("/books")
    public List<Book> getBookList() {
        List<Book> allBooks = this.bookService.getAllBooks();
        return allBooks;

    }

    @GetMapping("books/{bookId}")
    public ResponseEntity<Book> getBookById(@PathVariable("bookId") Long bookId) {
        try {
            Book book = this.bookService.getBookById(bookId);
            // Statut 200 OK avec le livre en réponse
            return ResponseEntity.ok(book);
        } catch (EntityNotFoundException exception) {

            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/books")
    public Book postBook(@RequestBody Book book) {
        return this.bookService.createBook(book);
    }

    @PutMapping("books/{id}")
    public Book updateBook(@PathVariable Long id, @RequestBody Book bookDetails) {
        return bookService.updateBook(id, bookDetails);
    }

    @DeleteMapping("books/{id}")
    public ResponseEntity<String> deleteBook(@PathVariable Long id) {
        bookService.deleteBook(id);
        String message = "Le livre avec l'ID " + id + " a été supprimé avec succès.";
        return ResponseEntity.ok(message);
    }

    @GetMapping("books/title/{title}")
    public ResponseEntity<Book> findByTitle(@PathVariable String title) {
        Optional<Book> optionalBook = bookService.findByTitle(title);
        if (optionalBook.isPresent()) {
            return ResponseEntity.ok(optionalBook.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // associer une categorie à un livre
    @PutMapping("{bookId}/categories/{id}")
    public ResponseEntity<Book> addCategoryToBook(@PathVariable Long bookId, @PathVariable Long id) {
        Optional<Book> bookOptional = this.bookRepository.findById(id);
        Optional<Category> categoryOptional = this.categoryRepository.findById(id);
        if (bookOptional.isPresent() && categoryOptional.isPresent()) {
            Book book = bookOptional.get();
            Category category = categoryOptional.get();
            book.setCategory(category);
            return ResponseEntity.ok(this.bookRepository.save(book));

        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
