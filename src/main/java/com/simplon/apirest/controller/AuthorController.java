package com.simplon.apirest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simplon.apirest.entity.Author;
import com.simplon.apirest.repository.AuthorRepository;

import jakarta.persistence.EntityNotFoundException;

import java.util.List;

@RestController
@RequestMapping("authors")
public class AuthorController {

    private final AuthorRepository authorRepository;

    public AuthorController(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    // Endpoint pour récupérer tous les auteurs
    @GetMapping
    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
       
    }

    // Endpoint pour récupérer un auteur par son ID
    @GetMapping("/{id}")
    public Author getAuthorById(@PathVariable Long id) {
        return authorRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Aucun auteur avec l'ID : " + id));
    }

    // Endpoint pour ajouter un nouvel auteur
    @PostMapping("/add")
    public Author addAuthor(@RequestBody Author author) {
        return authorRepository.save(author);
    }
}
