package com.simplon.apirest.entity;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.JoinColumn;

@Entity
// @Table : annotation utilisée pour spécifier le nom de la table dans la base
// de données où l'entité Book sera mappée
@Table(name = "Book")
public class Book {
    // L’annotation @Id spécifie que id est la clé primaire.
    @Id
    // L’annotation @GeneratedValue avec GenerationType.IDENTITY permet d’avoir une
    // clé primaire auto incrémentée.
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bookId;

    @Column(length = 100, nullable = false)
    private String title;

    @Column(columnDefinition = "TEXT")
    private String description;

    private boolean available = true;

    @ManyToOne
    @JoinColumn(name = "category_id")
    @JsonIgnoreProperties("books")

    private Category category;

    @ManyToMany
    @JoinTable(name = "book_author",
            // annotation @JoinColumn pour spécifier la colonne de jointure pour la relation
            // ManyToOne
            joinColumns = @JoinColumn(name = "book_id"), inverseJoinColumns = @JoinColumn(name = "author_id"))

    private Set<Author> authors = new HashSet<>();

    // Constructeur par défaut
    public Book() {
    }

    public Book(String title, Category category) {
        this.title = title;
        this.category = category;
    }

    // Getters et setters
    public Long getBookId() {
        return this.bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getAvailable() {
        return this.available;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Set<Author> getAuthor() {
        return this.authors;
    }

    public void setAuthor(Set<Author> authors) {
        this.authors = authors;
    }

    public Category getCategory() {
        return this.category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
