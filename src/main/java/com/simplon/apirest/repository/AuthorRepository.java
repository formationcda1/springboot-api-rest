package com.simplon.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.simplon.apirest.entity.Author;

public interface AuthorRepository extends JpaRepository<Author, Long>{
    
}
