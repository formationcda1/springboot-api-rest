package com.simplon.apirest.repository;

import org.springframework.stereotype.Repository;

import com.simplon.apirest.entity.Category;



import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Long>{
    //public List <Category> findByNameContainingIgnoreCase (String name);
}
