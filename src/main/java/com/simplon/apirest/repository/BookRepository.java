package com.simplon.apirest.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.simplon.apirest.entity.Book;
//Repository permet d'interargir avec la base de donnée
@Repository
//JpaRepository permet d'hériter des methodes du CRUD
public interface BookRepository extends JpaRepository<Book, Long> {
    Optional<Book> findByTitle(String title);

    
} 